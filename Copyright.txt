This software ("bitart") is Copyright (C) 2021 Chris Reuter unless
otherwise marked and has been released under the terms of the GNU
Affero General Public License version 3.

This program contains code taken from other projects but still owned
by the author; that code may have an earlier copyright date.

bitart is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with Benoit Mandelbot.  If not, see
<https://www.gnu.org/licenses/>.

Be advised that the copyright holder considers using this software to
automatically post images to a social network to be "Remote Network
Interaction" as referenced in section 13 of the license.

