# bitart - A procedural integer art generator

Inspired by [this post](https://www.metafilter.com/192164/Patterns),
bitart creates procedural art by generating a random integer function
that takes the coordinates of the current pixel and produces a result
that gets plotted, making some effort to reject "boring" images.

It does this without resorting to evaluating strings as Ruby code.

Here are some samples:

![Example 1](examples/1.png)

![Example 2](examples/2.png)

![Example 3](examples/3.png)

![Example 4](examples/4.png)

![Example 5](examples/5.png)

![Example 6](examples/6.png)

![Example 7](examples/7.png)


## Installation

`bitart` requires

* Ruby 3.0.0 or later (2.7.0 also seems to work, but isn't tested
  well)
* [libGD](https://libgd.github.io/) (should be available via your
  preferred package manager).
* The [gd2-ffij gem](https://github.com/dark-panda/gd2-ffij)

Simply install Ruby 3.0.x with your preferred versioning tool, the
libGD package and the gem with `gem install gd2-ffij` and you should
be good to go.

The `Rakefile` will run some of the unit tests.  (These are not rspec
tests; they use the minimal framework in `lib/assert.rb`.  (Don't
judge me.))

## Running

    ./bin/bitart

(This requires bash; if you don't have it installed or are using
Windows, you can also run it with

    ruby -I lib lib/main.rb

To get a list of available options, do

    ./bin/bitart --help









