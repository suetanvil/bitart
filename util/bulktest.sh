#!/bin/bash

set -e

root=$(cd $(dirname $0); pwd)

[ -d good ] || mkdir good
[ -d bad ] || mkdir bad

for ((c=100; c < 2000; c++)); do
    
    $root/bin/bitart -m 5 -k -o output

    c=$((c + 1))

    if grep -qE '^:problem: *$' output.yaml; then
        dest=good
    else
        dest=bad
    fi
    echo "$dest"
    
    mv output.png $dest/output$c.png
    mv output.yaml $dest/output$c.yaml
done

    
