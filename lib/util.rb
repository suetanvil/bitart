# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

# Various helper functions.

require 'base64'
require 'zlib'


# Return a new Struct instance whose fields are the given keys, set to
# the corresponding values.
def new_struct(**args)
  keys, values = args.to_a.transpose
  return Struct.new(*keys).new(*values)
end


# Compress str and encode the result as base64
def crunch64(str)
  result = Zlib::Deflate.deflate(str, Zlib::BEST_COMPRESSION)
  result = Base64.urlsafe_encode64(result)
  result.gsub!(/\s/, '')

  return result
end
