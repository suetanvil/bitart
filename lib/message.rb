# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

# Class and functions to provide silencable message printing.

require 'assert'

class MessagePrinter
  LEVELS = {
    quiet: 0,
    normal: 1,
    verbose: 2,
    debug: 3,
  }.freeze

  private
  
  def say(minlvl, msg)
    return false unless @verbosity >= LEVELS[minlvl]
    puts msg.map{|m| m.to_s}.join(" ")
    return true
  end
  
  public

  def initialize
    @verbosity = LEVELS[:normal]
  end

  def verbosity=(lvl)
    check("Unknown verbosity level: #{lvl}") { LEVELS.has_key? lvl }
    @verbosity = LEVELS[lvl]
  end

  def verbosity
    return LEVELS.key(@verbosity)
  end
  
  # Methods to explicitly set the verbosity level
  LEVELS.keys.each{|k|
    key = k     # Want this in a local var so the define_method will capture it.
    define_method("#{k}!".intern) { self.verbosity = key }
    define_method("#{k}?".intern) { LEVELS[key] == @verbosity }
  }

  def info(*msg)    return say(:normal, msg);   end
  def blabber(*msg) return say(:verbose, msg);  end 
  def dbg(*msg)     return say(:debug, msg);    end 
  def errmsg(*msg)  return say(:quiet, msg);    end
end

Console = MessagePrinter.new
%i{info blabber dbg errmsg}.each{|m|
  mm = m
  define_method(mm) { |*args| Console.send(mm, *args) }
}
