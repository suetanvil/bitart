# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

# Class to compute an image from a generated function and then render
# it, possibly after first validating it looks sufficiently
# interesting and trying again if it doesn't.
#
# We use a class instead of a collection of functions in order to
# provide a place to keep all of the parameters.

require 'gd2-ffij'
require 'set'

require 'grid'

EXTENT = 512
MAX_ZOOM = 3


class ComputeContext

  def initialize(depth:,
                 attempts:,
                 reject_bad: true,
                 scale_power: 0)
    @depth = depth
    @attempts = attempts
    @reject_bad = reject_bad
    @scale = 1 << scale_power
    @extent = EXTENT / @scale

    check("@scale too high!") { scale_power <= MAX_ZOOM }
  end


  def compute_and_render
    pixels, stats, fn, modulo = [nil, nil, nil]
    for attempt in 1 .. @attempts

      modulo = ( (2..13).to_a + [nil] ).sample
      fn = FunctionMaker.new(depth: @depth).make(modulo)

      blabber "Attempt #{attempt}/#{@attempts}: #{fn}"
      pixels = compute(fn.to_proc)
      stats = pixels.analysis()

      problem = review_image(pixels, stats)
      break unless @reject_bad && problem
      info "Rejecting #{attempt} (of #{@attempts}): #{problem}"

      if attempt == @attempts
        errmsg "Unable to produce an interesting pattern."
      return []
      end
    end

    color_fn = choose_color_function(stats, modulo)
    image = render(pixels, color_function(color_fn, stats))

    return [image, fn, stats, color_fn, modulo, problem]
  end


  private

  def compute(function)
    results = Grid.new(@extent, @extent)
    results.map!{ |x, y| function.call({x: x, y: y}) }
    return results
  end


  def render(pixels, color_fn)
    image = GD2::Image::TrueColor.new(@extent*@scale, @extent*@scale)
    canvas = GD2::Canvas.new(image)

    blabber "Drawing..."
    pixels.each_pos do |x, y, val|
      canvas.color = color_fn.call( val )
      canvas.rectangle(x*@scale, y*@scale, (x+1)*@scale, (y+1)*@scale, true)
    end

    return image
  end


  def stripes_count(pixels)
    max_pattern = 16
    fraction = 0.95

    hcount = vcount = 0

    for stripe_pos in 0..@extent-1
      vcount += 1 if pixels.repeated_pattern(stripe_pos,
                                             vertical: true,
                                             maxlen: max_pattern)
      hcount += 1 if pixels.repeated_pattern(stripe_pos,
                                             vertical: false,
                                             maxlen: max_pattern)
    end

    striped = vcount.to_f / @extent > fraction ||
              hcount.to_f / @extent > fraction

    return [striped, hcount, vcount]
  end


  def review_image(pixels, hist)
    return "Solid colour" unless hist.num_keys > 1
    return "Dominance too high: #{hist.dominance}" if hist.dominance > 0.98

    striped, hcount, vcount = stripes_count(pixels)
    return "Image is mostly stripes (#{hcount}, #{vcount})" if striped

    return nil
  end


  def color_function(mode, stats)
    bw_palette = [GD2::Color::BLACK, GD2::Color::WHITE]

    min_key = stats.min_key
    max_key = stats.max_key
    most_common_key = stats.most_common_key

    functions = {
      onebit:     proc{ |n| bw_palette[n == most_common_key ? 0 : 1] },
      gradient:   proc{ |n|
        mag = (n - min_key) / (max_key - min_key).to_f
        mag = 0 unless mag.finite?        # catch div-by-zero
        GD2::Color.new(mag, mag, mag)
      }
    }

    return functions[mode]
  end


  def choose_color_function(stats, modulo)
    return :onebit if modulo  # || stats.num_keys < 9
    return :gradient
  end
end
