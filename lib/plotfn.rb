# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

# Classes used to implement the plotting function.
#
# A function consists of a tree of PlotFn subinstances, each
# representing a specific type of subexpression.  It can produce
# either a textual description (via '.to_s') or a proc (via
# '.to_proc') that will compute the expression.
#
# The proc takes one argument, the context.  This is a hash that maps
# symbols to integers; instances of Lookup will retrieve the value
# corresponding to their symbols from this hash where they can be used
# by the parent(s).

require 'assert'

class PlotFnError < RuntimeError; end
class PFTypeError < PlotFnError; end


# We monkeypatch division and modulo functions that also define zero
# divisors; this lets us do division without having to worry about
# division by zero errors.
class Integer
  def safe_div(divisor)
    return 1    if divisor == self      # semi-graceful for 0/0
    return -1   if divisor == 0         # ditto for div by zero
    return self / divisor
  end

  def safe_mod(divisor)
    return 0 if divisor == 0
    return self % divisor
  end
end



# The abstract base class.
class PlotFn

  def lookup?()     return false; end
  def literal?()    return false; end
  def binary?()     return false; end
  def unary?()      return false; end
  def expression?() return false; end


  def to_s
    raise "Subclass responsibility."
  end

  def inspect
    raise "Subclass responsibility."
  end

  def to_proc
    raise "Subclass responsibility."
  end

  # Helper function to create the appropriate corresponding PlotFn
  # sub-instance for 'obj'.  'obj' can be an integer or a symbol
  # (representing a variable lookup).  (A PlotFn subinstance is also
  # allowed; it just gets returned.)
  def self.wrap(obj)
    return obj                   if obj.class <= PlotFn
    return Literal.new(obj)      if obj.class == Integer
    return Lookup.new(obj)       if obj.class == Symbol
    raise PFTypeError.new "Unwrappable object '#{obj}' (class #{obj.class})"
  end

  # This is here mostly for testing
  def eval(context = {})
    return self.to_proc.call(context)
  end

  protected

  def wrap(obj)
    return self.class.wrap(obj)
  end
end


# A unary or binary expression
class Expression < PlotFn
  # Supported binary and unary operators.
  BIN_OPS = %i{+ - * & | ^ / %}
  UN_OPS = %i{-@ ~}

  attr_reader :operator, :lhs, :rhs

  def initialize(operator, arg1, arg2 = nil)
    @operator = operator
    @binary = !!arg2
    @lhs = @binary ? wrap( arg1 ) : nil
    @rhs = @binary ? wrap( arg2 ) : wrap( arg1 )

    assert("Unknown binary operator '#{operator}'") {
      !@binary or BIN_OPS.include? operator
    }
    assert("Unknown unary operator: '#{operator}'") {
      @binary or UN_OPS.include? operator
    }
  end

  public

  def binary?()     return @binary;     end
  def unary?()      return !@binary;    end
  def expression?() return true;        end

  def inspect
    args = [@operator]
    args.push @lhs if @binary
    args.push @rhs

    args_str = args.map{|a| a.inspect}.join(", ")
    return "#{self.class}.new(#{args_str})"
  end

  def to_s
    op = @operator.to_s.gsub(/\@/, '')

    return "#{bracket(@lhs)} #{op} #{bracket(@rhs)}" if @binary
    return "#{op}#{bracket(@rhs)}"
  end

  private

  # Return 'plotfn' as a string, surrounded by parentheses if
  # appropriate.
  def bracket(plotfn)
    return "(#{plotfn})" if plotfn.binary? || plotfn.unary?
    return plotfn.to_s
  end

  public

  # Return a proc that evaluates this expression.  Division operations
  # ('/' and '%') are replaced by the zero-safe versions defined
  # above.
  def to_proc
    arg_procs = [@rhs.to_proc]
    arg_procs.unshift @lhs.to_proc if @binary

    op = @operator
    op = :safe_mod if op == :%
    op = :safe_div if op == :/

    return proc {|context|
      # Evaluate the arguments
      args = arg_procs.map{|arg| arg.call(context)}

      # apply the operator
      rcvr = args.shift
      next rcvr.send(op, *args)
    }
  end
end


# An Integer literal
class Literal < PlotFn
  def initialize(int)
    @value = int
  end

  def literal?()        return true;                            end
  def to_s()            return "#{@value}";                     end
  def inspect()         return "Literal.new(#{@value})";        end

  def to_proc
    v = @value
    return proc{|context| v }
  end
end


# A variable lookup; the variable must be defined to an integer in the
# has passed to the proc.
class Lookup < PlotFn
  def initialize(name)
    @name = name
  end

  def lookup?()         return true                             end
  def to_s()            return "#{@name}"                       end
  def inspect()         return "Lookup.new(#{@name.inspect})";  end

  def to_proc
    nm = @name
    return proc{|context| context[nm]}
  end
end


# Helper function used for testing. Creates a PlotFn node; which node
# is determined bythe type and number of arguments.
def op(*args)
  return PlotFn.wrap(args[0]) if args.size == 1
  return Expression.new(*args)
end

spec {
  context = {x: 3, y: 13}

  o = op(42)
  assert{ o.eval == 42 }
  assert{ o.to_s == "42" }

  o = op(:-@, 1)
  assert{ o.eval == -1 }
  assert{ o.to_s == "-1" }

  o = op(:x)
  assert("test lookup"){ o.eval(context) == 3 }
  assert("test printing"){ o.to_s == "x" }


  o = op(:*, op(:|, :x, :y), 2)
  assert("test expr eval"){ o.eval(context) == 30 }
  assert("test expr eval"){ o.eval({x: 42, y: 42}) == 84 }
  assert("test expr printing") { o.to_s.gsub(/\s/, '') == '(x|y)*2' }

  o = op(:~, 1)
  assert("test ~1"){ o.eval(context) == ~1 }
}
