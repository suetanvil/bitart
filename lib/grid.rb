# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

require 'assert'


# Class to hold a two-dimensional array of integers and do a certain
# amount of analysis on them.  Gets used to hold the computed pixel
# values before they are rendered.
class Grid
  attr_reader :width, :height

  def initialize(width, height)
    @width = width
    @height = height
    @points = []

    fill!(0)
  end

  def inspect
    return "Grid.new(#{@width},#{@height})"
  end

  private

  def idx(x, y)
    check("X value #{x} is out of bounds!") { x < @width }
    check("Y value #{y} out of bounds!") { y < @height }
    return x + (@width * y)
  end

  public

  def [](x, y)
    return @points[ idx(x, y) ]
  end

  def []=(x, y, value)
    return @points[ idx(x,y) ] = value
  end

  def fill!(value)
    @points = [value] * (@width * @height)
    return value
  end

  def each_pos(&block)
    for x in 0..@width - 1
      for y in 0..@height - 1
        block.call(x, y, self[x,y])
      end
    end
  end

  def each(&block)
    each_pos {|x, y, val| block.call(val) }
  end

  def map!(&block)
    each_pos{ |x, y, value| self[x, y] = block.call(x, y, value) }
  end

  # Return a histogram of pixel values.
  def histogram
    hist = {}
    self.each_pos{ |x, y, val| hist[val] = (hist[val] || 0) + 1 }
    return hist
  end

  # Compute various results from the image and return them.
  def analysis
    hist = self.histogram

    # Number of distinct pixel values plus their minimum and maximum.
    num_keys = hist.keys.size
    min_key = hist.keys.min
    max_key = hist.keys.max

    # Most common pixel value and how often it appears
    most_common_key_count = (hist.values.sort)[-1]
    most_common_key = hist
                        .to_a
                        .select{|key, value| value == most_common_key_count}
                        .first
                        .at(0)

    # Ratio of number of distinct pixel values to the range of values
    # between minimum and maximum.
    density = num_keys.to_f / (hist.keys.max.to_f - hist.keys.min + 1)

    # Fraction of pixels containing the most common value.
    dominance = hist.values.max.to_f / hist.values.sum

    return new_struct(
             num_keys:              num_keys,
             min_key:               min_key,
             max_key:               max_key,

             # Most common key and the number of times it appears
             most_common_key:       most_common_key,
             most_common_key_count: most_common_key_count,

             # Fraction of slots in the key range that are populated.
             density:               density,

             # Fraction of the keys that are the most common key
             dominance:             dominance,
           )
  end


  # Search for a repeating pattern along a strip of the grid at
  # position 'index'.  Return the pattern if found; nil otherwise.
  def repeated_pattern(index, vertical: true, maxlen: 8)
    stripe = vertical                                    ?
               (0..@height - 1).map{|y| self[index, y]}  :
               (0..@width  - 1).map{|x| self[x, index]}

    return find_pattern_in(stripe, maxlen)
  end

  private

  def find_pattern_in(stripe, max_pattern_length)
    assert{ max_pattern_length < stripe.size }

    pattern = [ stripe[0] ]

    while true
      idx = repeats_to(stripe, pattern)
      return pattern if idx + pattern.size > stripe.size

      pattern = stripe[0..idx]
      return nil if pattern.size > max_pattern_length
    end
  end

  # Return the index of the first item in stripe that is not a repetition
  # of 'pattern'.
  def repeats_to(stripe, pattern)
    for n in 0..stripe.size - 1
      return n unless stripe[n] == pattern[n % pattern.size]
    end

    return stripe.size
  end

end


# Unit tests for Grid.
spec("grid tests") {
  g = Grid.new(20, 10)
  g.fill!(42)

  # Basic lookups
  assert("lookup 0,0") { g[0,0] == 42 }
  assert("lookup 19,9") { g[19,9] == 42 }
  assert("lookup ") { g[2,6] == 42 }

  g.map!{|x, y| x % (y + 1)}
  g.each_pos{|x, y, val|
    assert("each lookup #{[x,y,val]}") { val == x % (y + 1) }
  }

  # Repetitive pattern detection
  g = Grid.new(20, 20)
  g.fill!(42)

  # Solid pattern
  (0..19).each { |i|
    assert("solid hpat #{i}") { g.repeated_pattern(i, vertical: false) }
    assert("solid vpat #{i}") { g.repeated_pattern(i, vertical: true) }
  }

  # no pattern
  g[2,2] = 0
  assert("non-pat 1") { g.repeated_pattern(2, vertical: false) == nil }

  g.fill!(0)
  g[2,17] = 1
  assert("non-pat 2") {
    g.repeated_pattern(2, vertical: true) == nil
  }


  # alternating pattern
  for diff in 1..16
    g.fill!(0)
    g.map!{|x, y| (x % diff == 0 && y % diff == 0) ? 0 : 1 }

    (0..19).each { |i|
      pat = g.repeated_pattern(i, vertical: false, maxlen: 16)
      assert("speckled hpat #{diff} - #{i} '#{pat}'") {
        pat
      }

      pat = g.repeated_pattern(i, vertical: true, maxlen: 16)
      assert("speckled vpat #{diff} -  #{i} '#{pat}'") {
        pat
      }
    }
  end





}
