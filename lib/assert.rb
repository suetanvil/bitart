# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

# Sanity check utility: Evaluates a block and throws an exception if
# it fails.  Also provides lightweight unit testing.

class AssertFailure < RuntimeError; end
class CheckFailure < RuntimeError; end

module Assertions
    
  # Disable debug-time assertions.  These are on by default and can
  # only be switched off.
  def self.no_debug!
    @no_debugging = true
  end

  # Throw 'exception' with 'message' unless @no_debugging is true (and
  # 'always' is false) or &block evaluates to true. '&block' is not
  # evaluated if the earlier condition is met.
  def self.throw_unless(exception, message, &block)
    return if block.call()

    message = [exception.name] if message.size == 0
    message = message.map{|s|s.to_s}.join(" ")
    raise exception.new(message)
  end

  # Evaluate &block.  If the result is false, throws AssertFailure
  # with the `*message` arguments converted to strings and
  # concatenated together with spaces as the error message.
  def self.assert(*message, &block)
    throw_unless(AssertFailure, message, &block)
  end

  # Like `assert` but does nothing (including not evaluating &block)
  # if 'no_debug!' has been called in the past.
  def self.assert_dbg(*message, &block)
    return if @no_debugging
    throw_unless(AssertFailure, message, &block)
  end

  # Like `assert` but throws CheckFailure instead of AssertFailure.
  # Use this instead of `assert` for cases where the problem is not
  # caused by a bug in your code (e.g. file not found).
  def self.check(*message, &block)
    throw_unless(CheckFailure, message, &block)
  end

  # Assert that evaluating the block will throw an exception.
  #
  # Arguments (optional):
  #     - A string, containing the message to print if the assertion fails.
  #       If ommitted, a default is used.
  #     - Zero or more exceptions to check for.  If none or given, defaults
  #       to RuntimeError
  #       
  def self.assert_thrown(*args, &body)
    msg = nil
    exceptions = []

    args.each do |arg|
      if arg.is_a? String
        msg = arg
      elsif arg.is_a?(Class) && arg <= Exception
        exceptions.push arg
      else
        raise CheckFailure.new("Invalid argument: '#{arg}'")
      end
    end

    exceptions.push RuntimeError if exceptions.empty?
    msg = "Expecting (one of) exception(s) #{exceptions.join(', ')}" unless msg

    begin
      body.call()
    rescue => e
      raise AssertFailure.new("UNEXPECTED EXCEPTION: '#{e}'\n#{msg}") if
        exceptions.select{|expected| e.is_a? expected}.size == 0
      return
    end

    raise AssertFailure.new(msg)
  end

  
  # Disable 'spec' evaluation
  def self.spec_off!
    @disable_spec = true
  end

  # Backend for 'spec'
  def self.run_spec(desc, body, clr)
    # Skip this spec_off! was called or if this wasn't called from the
    # source file's mainline routine.
    return true if @disable_spec || clr.path != $0
    
    desc = ": #{desc}" if desc
    puts "spec#{desc}"

    @no_debugging = false
    body.call
  rescue CheckFailure, AssertFailure => e
    puts "Failure: #{e}"
    puts e.backtrace
    exit(1)
  end
end

# Lightweight unit test.  Evaluate 'body' if called from ruby's input
# file (i.e. the main source file) and spec_off! has not been called.
# Exit if any assertions failed.
def spec(desc = nil, &body)
  Assertions.run_spec(desc, body, caller_locations()[0])
end


# Glue functions.
def assert(*args, &block)       Assertions.assert(*args, &block);       end
def assert_dbg(*args, &block)   Assertions.assert_dbg(*args, &block);   end
def check(*args, &block)        Assertions.check(*args, &block);        end
def spec_off!()                 Assertions.spec_off!;                   end
def assert_thrown(*args, &body) Assertions.assert_thrown(*args, &body); end
