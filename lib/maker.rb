# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

# Class to procedurally construct the colouring function
class FunctionMaker
  def initialize(unary_rate: 0.3,
                 literal_rate: 0.5,
                 max_literal: 24,
                 depth: 3)
    @max_literal = max_literal
    @literal_rate = literal_rate
    @unary_rate = unary_rate
    @depth = depth
  end

  def make(modulo = nil)
    fn = make_func(@depth)
    fn = Expression.new(:%, fn, PlotFn.wrap(modulo)) if modulo
    return fn
  end

  private

  def make_leaf(force_lookup)
    return Lookup.new( %i{x y}.sample ) if
      force_lookup || rand() < @literal_rate

    return Literal.new( rand(@max_literal) + 1 )
  end

  def make_func(depth, force_lookup = true)
    return make_leaf(force_lookup) if depth == 0
    return make_unary(depth) if rand() < @unary_rate
    return make_binary(depth)
  end

  def make_unary(depth)
    op = Expression::UN_OPS.sample
    arg = make_func(depth - 1)
    return Expression.new(op, arg)
  end

  def make_binary(depth)
    op = Expression::BIN_OPS.sample
    left = make_func(depth - 1, true)
    right = make_func(depth - 1, false)

    left, right = [left, right].shuffle

    return Expression.new(op, left, right)
  end
end

