# bitart, Copyright (C) 2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.


require 'gd2-ffij'
require 'optparse'
require 'yaml'

require 'plotfn'
require 'maker'
require 'compute'
require 'message'
require 'util'

DEFAULT_ZOOM = 1



def getopts
  opts = new_struct(
    filename: nil,
    depth: 4,
    no_meta: false,
    command: nil,
    max_depth: nil,
    reject_bad: true,
    quiet: false,
    zoom: nil,
  )

  OptionParser.new do |opo|
    opo.banner = "Usage: bitart.rb [options]"

    opo.on("-o", "--output FILE",
           "Output filename; defaults to base-64-encoded equation text."
          ) { | filename |
      filename = "#{filename}.png" unless filename =~ /\.png/
      opts.filename = filename
    }

    opo.on("-d", "--depth NUM", Integer, "Set equation depth.") { |d|
      check("Depth #{d} is less than 1.") { d > 0 }
      opts.depth = d
    }

    opo.on("-m", "--max-depth NUM", Integer,
           "Enable random depth selection to the given maximum .") { |d|
      check("Depth #{d} is less than 1.") { d > 0 }
      opts.max_depth = d
    }

    opo.on("-i", "--no-meta",
           "Suppress the '<filename>.yaml' description file.") {
      opts.no_meta = true
    }

    opo.on("-r", "--run COMMAND",
           "Execute 'command' followed by the output filename on completion.") {
      |cmd|
      opts.command = cmd
    }

    opo.on("-k", "--keep", "Keep the first image, regardless of quality.") {
      opts.reject_bad = false
    }

    opo.on("-q", "--quiet", "Quiet output.") {
      opts.quiet = true
    }

    opo.on("-z", "--zoom POWER", Integer,
           "Zoom power (default is random)") { |zoom|
      check("Zoom must be between 0 and #{MAX_ZOOM}") {
        zoom >= 0 && zoom <= MAX_ZOOM
      }
      opts.zoom = zoom
    }
  end.parse!
  return opts
end



def make_metadata(fn, stats, mode, modulo, depth, problem, zoom)

  fn_desc = "f(x,y) = #{fn.to_s}"
  fn_serialized = crunch64(YAML.dump(fn))
  scale = 1 << zoom
  
  result = {
    equation:       fn_desc,
    eqn_serialized: fn_serialized,
    depth:          depth,
    color_mode:     mode,
    modulo:         modulo,
    problem:        problem,
    scale:          scale,
    extent:         EXTENT / scale,
  }

  result.merge!(stats.to_h)

  return result
end

def main()
  opts = getopts()

  Console.verbosity = opts.quiet ? :quiet : :verbose
  
  opts.depth = [2, rand(opts.max_depth - 1)].max if opts.max_depth
  info "Depth: #{opts.depth}" if opts.max_depth

  # Choose zoom level: user settings if given, otherwise the default
  # 80% of the time and a random option the rest.
  zoom = opts.zoom || DEFAULT_ZOOM
  zoom = rand(MAX_ZOOM + 1) if !opts.zoom && rand() < 0.2
  info "Zoom power: #{zoom} (#{opts.zoom ? 'user set' : 'random'})"

  cc = ComputeContext.new(depth: opts.depth,
                          attempts: 20,
                          reject_bad: opts.reject_bad,
                          scale_power: zoom)
  image, fn, stats, color_fn, modulo, problem = cc.compute_and_render()
  exit 1 unless image

  info "Function: f(x,y) := #{fn.to_s}"

  filename = opts.filename
  if ! filename
    filename = crunch64( fn.to_s.gsub(/\s/, '') ) + ".png"
  end

  info "Saving to #{filename}..."
  image.export(filename, level: 9)

  mdname = filename.gsub(/\.png$/, '.yaml')
  md = make_metadata(fn, stats, color_fn, modulo, opts.depth, problem, zoom)

  info "Metadata:"
  md.each_pair{|k, v| info "  #{k}: #{v}" }

  if ! opts.no_meta
    info "Writing info file #{mdname}..."
    IO.write(mdname, YAML.dump(md))
  end

  if opts.command
    system("#{opts.command} #{filename}")
  end
end


main()
